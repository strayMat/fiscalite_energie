[tool.poetry]
name = "fiscalite_energie"
version = "0.0.0"
description = "Fiscalite_Energie"
authors = ["Matthieu Doutreligne <matt.dout@gmail.com>"]
license = "EUPL-v1.2"
readme = "README.md"
repository = "https://gitlab.com/strayMat/fiscalite_energie"
homepage = "https://gitlab.com/strayMat/fiscalite_energie"
include = ["bin"]
keywords = []
classifiers = [
    "Intended Audience :: Developers",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: Implementation :: CPython",
    "Programming Language :: Python",
    "Topic :: Software Development :: Libraries :: Python Modules",
]

# [tool.poetry.urls]
# Changelog = "https://gitlab.com/strayMat/fiscalite_energie/releases"

[tool.poetry.dependencies]
python = "^3.8"

# Project-Specific
python-dotenv = "^0.20.0"
click = "^8.0.4"

[tool.poetry.group.documentation]
optional = true
[tool.poetry.group.documentation.dependencies]
# Documentation
importlib-metadata = { version = "^4.11.3", optional = false }
myst-parser = { version = "^0.17.0", optional = false }
pygments = { version = "^2.11.2", optional = false }
sphinx = { version = "^4.4.0", optional = false }
sphinx-autodoc-typehints = { version = "^1.17.0", optional = false }
pydata-sphinx-theme = "0.11.0"
sphinxcontrib-apidoc = { version = "^0.3.0", optional = false }
sphinx-click = { version = "^3.1.0", optional = false }
sphinxcontrib-bibtex = { version = "^2.5.0", optional = false }
sphinx-design = "^0.3.0"



[tool.poetry.group.dev.dependencies]
# Linting
## Type Checking and Data Validation
mypy = "^0.942" # Static type checker
## Code formatting
black = "^22.1.0" # see: https://black.readthedocs.io/en/stable/editor_integration.html
## Code quality
isort = "^5.10.1"
pylint = "^2.13.0"
## Detect secrets
detect-secrets = "^1.4.0"
## Security Issues
bandit = "^1.7.4"
## Doc string
interrogate = "^1.5.0"
## Automation and management
pre-commit = "^2.17.0"
## Upgrade syntax for newer versions of Python
pyupgrade = "^3.3.1"
## Linter for YAML files
yamllint = "1.29.0"
## various convenient hooks
pre-commit-hooks = "^4.4.0"

[tool.poetry.scripts]
cli = "bin.cli:cli"

#################################################################################
# Tooling configs                                                               #
#################################################################################
[tool.bandit]
exclude_dirs = ["tests/", ".*$"]

[tool.black]
line-length = 79
include = '\.pyi?$'

[tool.coverage.run]
branch = true
concurrency = ["multiprocessing"]
parallel = true
source = ["fiscalite_energie"]

[tool.coverage.report]
exclude_lines =[
    "pragma: no cover",
    "raise AssertionError",
    "raise NotImplementedError",
    "if __name__ == .__main__.:",
]
fail_under = 70
show_missing = true
skip_covered = true

[tool.interrogate]
verbose = 0
quiet = false
fail-under = 0
color = true

[tool.isort]
profile = "black"
atomic = "true"
combine_as_imports = "true"
line_length = 79

[tool.mypy]
disallow_untyped_defs = false
files = ["fiscalite_energie/*.py","bin/*.py"]
ignore_missing_imports = true
pretty = true
show_column_numbers = true
show_error_context = true
show_error_codes = true

[tool.pylint.basic]
good-names-rgxs = ["^Test_.*$", "logger"]

[tool.pylint.messages_control]
disable = [
  # Explicitly document only as needed
  "missing-module-docstring",
  "missing-class-docstring",
  "missing-function-docstring",
  # Black & Flake8 purview
  "line-too-long",
  "c-extension-no-member",
]

[tool.pylint.reports]
output-format = "colorized"

[tool.pylint.similarities]
# Ignore imports when computing similarities.
ignore-imports = "yes"

[tool.cruft]
skip = [
    ".git",
    "README.md",
]

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
