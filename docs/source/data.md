# Données et protocole

## Données

### Producteurs intéressants

Le service statistique du ministère de l'environnement (SDES du MTES) disposent
de pas mal de données différentes comme par exemple ce [tableau de suivi de la rénovation énergétique dans le secteur résidentiel](https://www.statistiques.developpement-durable.gouv.fr/tableau-de-suivi-de-la-renovation-energetique-dans-le-secteur-residentiel): On y voit notamment une légère baisse (355 en 2012 à 330.8 TWH en 2021) de la consommation énergétique du secteur des logements sur 10 ans, associée à une forte diminution des émissions de CO2 du secteur (57.3 à 44.3 MtCO2), due à la baisse de l'utilisation des énergies fossiles.

- [Le catalogue de données de l'ADEME est bien fourni](https://data.ademe.fr/datasets?). J'ai listé ci-dessous en janvier 2024 les principales sources intéressantes pour l'étude de l'effet rebond. 

- Les données sur la thématique du logement [disponibles au CASD](https://www.casd.eu/donnees-utilisees-sur-le-casd/) ne
  semblent pas contenir d'information sur les consommations d'énergie (enquête logement, ou FIDELI). 

### Catalogue détaillé

```{eval-rst}
.. csv-table:: Catalogue de données.
        :file: /_static/data_catalog.csv
        :widths: 15, 15, 20, 20, 20, 10
        :header-rows: 1

```

### Autres données potentiellement intéressantes

- TREMI : [TREMI 2017 en open data](https://www.data.gouv.fr/fr/datasets/enquete-tremi-2017/) ou [TREMI 2020 sur le CASD](https://www.casd.eu/source/enquete-sur-les-travaux-de-renovation-energetique-des-maisons-individuelles/): L'enquête récente (2020) comporte une estimation de la consommation d'énergie avant/après travaux mais malheureusement, ça a l'air basée sur une modélisation (donc pas de données réelles) et n'inclue donc pas l'effet rebond. En revanche ces données sont intéressantes pour décrire les caractéristiques des personnes ayant effectuées des travaux. Les résultats ont été présentée dans [l'analyse de l'enquête par le SDES du MTES](https://data.ademe.fr/data-fair/api/v1/datasets/tremi-2017-resultats-bruts/metadata-attachments/TREMI_2017_brochure_synthese.pdf).

- [Consommation énergétique du secteur résidentiel par usage, 1990 à 2021](https://www.statistiques.developpement-durable.gouv.fr/consommation-denergie-par-usage-du-residentiel)


## Premières réflexions sur le protocole d'étude

- Pour une étude simple par série chronologique, on se fixe comme objectif de regarder l’association entre le nombre de travaux et la consommation énergétique du logement. Les données agrégées de consommation sont disponibles sur le site du SDES ([sûrement commencer ici](https://www.statistiques.developpement-durable.gouv.fr/tableau-de-suivi-de-la-renovation-energetique-dans-le-secteur-residentiel)). Même dans une approche agrégée, il va être intéressant de contrôler par les caractéristiques des logements (surface en m2, type d'installation, de travaux, ...). Dans ce cas, essayer de s'inspirer des méthodes de [causalImpact](https://google.github.io/CausalImpact/CausalImpact.html) même si j'ai peur que le côté agrégée noie plein d'effets les uns avec les autres. Les sources détaillées par enerdata sont certainement suffisantes pour cette partie de l'étude.

- Pour une étude fine prenant en compte les confondants et les caractéristiques des logements, il est nécessaire d'adopter une approche causale avec une stratégie d'identification de l'effet : **Quelle est l'effet sur la consommation d’énergie des travaux de rénovations énergétique?**. Les données de consommations d'électricité et de gaz pour la maille fine d'habitation [Iris](https://www.insee.fr/fr/metadonnees/definition/c1523) ont l'air disponible par le SDES : [Données locales de consommation d'électricité, de gaz naturel et de chaleur et de froid - IRIS (à partir de 2018)](https://www.statistiques.developpement-durable.gouv.fr/catalogue?page=dataset&datasetId=610244c9e436671e84ec5da8). En associant ces données à des données reflétant la mise en oeuvre plus ou moins importante de rénovations, on peut faire une étude en matchant des iris similaires et en prenant comme *traitement* des quantiles de logements rénovés. A défaut, on peut utiliser les [données (par adresse?) de consommation DPE](https://data.ademe.fr/datasets/dpe-france) pour approcher par iris une quantité de rénovation. Malheureusement, on peut pas croiser les données de consommation d'électricité IRIS solution alternative de croiser les données de consommations par Iris à celles de l'enquête TREMi car ces dernières sont seulement à une maille départementale.