```{include} readme.md

```

```{eval-rst}
.. toctree::
   :hidden:
   :maxdepth: 1

   readme
   Données <data>
```

```{eval-rst}
.. role:: bash(code)
   :language: bash
   :class: highlight
```
