# Fiscalité énergétique du bâtiment

**Documentation**: [https://straymat.gitlab.io/fiscalite_energie](https://straymat.gitlab.io/fiscalite_energie)

**Source code**: [https://gitlab.com/strayMat/fiscalite_energie](https://gitlab.com/strayMat/fiscalite_energie)

---


Cette page dresse les objectifs de l'étude et un début de revue de littérature.

La page [Données](data) liste certaines données particulièrement intéressantes pour l'objectif principal ainsi que [de premières réflexions sur le protocole ou le design de l'étude](https://straymat.gitlab.io/fiscalite_energie/data.html#premieres-reflexions-sur-le-protocole-d-etude).

## Objectifs

### Objectif principal: Quantifier l'effet rebond dans le domaine de la rénovation énergétique

Y-a-t-il eu un effet rebond avec l'amélioration des performances énergétiques
des bâtiments? 

### Objectif secondaire: Caractériser les propriétaires rénovateurs

Qui sont les français qui rénovent le plus ? Les variables étudiées seront
idéalement: le revenu, l'âge, la localisation géographique, la surface du
logement, le type de chauffage, la facture énergétique, la date de construction.

## Précédents travaux

Cette page référence les travaux pertinents nécessaires afin d'étudier les caractéristiques de la rénovation énérgétique en France.

La page [data](data.md) contient la documentation des données les plus intéressantes pour l'objectif principal.

### Études et articles scientifiques

Listés dans par ordre de pertinence pour l'objectif d'étude.

- [Fowlie, M., Greenstone, M., & Wolfram, C. (2018). Do energy efficiency investments deliver? Evidence from the weatherization assistance program. The Quarterly Journal of Economics, 133(3), 1597-1644.](https://www.nber.org/system/files/working_papers/w21331/w21331.pdf):
  Papier de référence (NBER + référence d'Esther Duflo) étudiant l'impact de la rénovation énergétique sur la consommation individuelle à partir de 30 000 foyers au Michigan. Basé sur des données expérimentales (essai randomisé) et quasi-expérimentales, ils concluent que : a) la demande est faible parmi les foyers malgré des campagnes marketing massives (5% increase pour 1000 dollars dépensés par foyer) ; b) la réduction de la consommation d'énergie est de 10 à 20% alors que les projections basées sur les modèles d’ingénierie tablent sur 25 à 45%. Ce résultat a pour conséquence de changer les résultats de profitabilité de la rénovation énergétique de très positif à zéro [^1]. c) la température intérieure n'est pas significativement plus haute dans les foyers rénovés (pas d'effet rebond retrouvé).  
  
  En conclusion, ils soulignent que la rénovation énergétique
  doit être mesurée à partir des gains réelles d'énergie et non ceux projetés.
  En effet, les gains réels sont faibles voire négatifs sur leur population.

- [Blaise, G., & Glachant, M. (2019). Quel est l’impact des travaux de rénovation énergétique des logements sur la consommation d’énergie. La Revue de l’énergie, 646, 46-60.](https://www.cerna.minesparis.psl.eu/Donnees/data16/1674-Glachant-Blaise-Revue-Energie-646.pdf):
  Evaluatioon de l'effet des incitations économiques sur la réduction de la consommation d'énergie: L'auteur (ancien du collège des ingénieurs) conclue à une réduction de la facture énergétique de (8,29 euros de facture en moins/1000 euros dépensées). Ils se basent sur l'enquête [Maîtrise de l’Énergie «10 000 ménages» réalisée par TNS-SOFRES pour l’ADEME de 2000 à 2013](https://presse.ademe.fr/files/ip10000menages_18092013.pdf). L'étude emploie une méthode par G-formula (régression linéaire) et une méthode instrumentale utilisant la volonté pré-travaux de vouloir effectuer des travaux me paraît assez peu robuste. L'ensemble de la méthodologie économétrique est un peu sommaire mais c'est une des rares études à étudier exactement le retour sur investissement de la rénovation énergétique. Il me semble qu'elle ne prend pas en compte non plus l'inflation des prix de l'énergie. 

- [Efficacité économique et effets distributifs de long-terme des politiques de rénovation énergétique des logements, Louis-Gaëtan Giraudet, Cyril Bourgeois, Philippe Quirion, 2020](https://www.cairn.info/revue-economie-et-prevision-2020-1-page-43.htm) : Basé sur RES-IRF, un gros modèle paramétrique micro avec plein de paramètres que ce soit pour les caractéristiques des logements ou les modèles de projection, mais pas de données réelles. Conclue à une nécessité de ne pas dériver des objectifs les plus ambitieux. La taxe carbone est l'instrument le plus efficace, mais accroît la précarité énergétique.
 

- Effet rebond en Allemagne: 
    - Slides du GWD (Whonungs wirtschaft, syndicat des sociétés du bâtiment), [slides,  s.32](https://www.gdw.de/media/2020/07/jpk2020-praesentation-1.6-mit-kmt-o-bs-.pdf). Ils montrent une forte diminution de la consommation énergétique entre 1990 et 2010, puis une stagnation entre 2010 et 2018. Les investissements et actions prévoyaient pourtant une diminution de 15% sur cette dernière période. 
    - Cette présentation a été aussi reprise par un article du monde [Cécile Boutelet, « En Allemagne, les rénovations énergétiques des bâtiments n’ont pas fait baisser les  consommations », Le Monde, 4 octobre 2020](https://www.lemonde.fr/economie/article/2020/10/04/en-allemagne-les-renovations-energetiques-des-batiments-n-ont-pas-fait-baisser-la-consommation_6054715_3234.html).
    - Enfin un [article de l'IDDRI en janvier 2021 par Andreas Rüdinger](https://www.iddri.org/en/publications-and-events/blog-post/energy-renovation-germany-success-story-or-potential-failure)
      critique les conclusions de l'article du Monde en se basant sur des données de consommation du ministère de l'économie et de l'environnement allemand. Une critique particulièrement pertinente est que les analyses ne portent que sur le parc aggrégé de logements et non sur les logements
      rénovés. (L'[IDDRI](https://fr.wikipedia.org/wiki/Institut_du_d%C3%A9veloppement_durable_et_des_relations_internationales) est un think tank indépendant mais je ne comprends pas trop qui le finance).

  - [Rapport d'un cabinet d'études (CREDOC) après une enquête de 2009](https://www.credoc.fr/publications/la-temperature-du-logement-ne-depend-pas-de-la-sensibilite-ecologique-house-temperature-and-ecological-sensibility): la température jugée confortable dans le séjour par les ménages français était fortement dépendante de la date de construction de leur logement (corrélée donc aux performances d'isolation).


### Autres références de contexte

Cette liste de références permet de mieux comprendre le contexte, mais n'est pas directement pertinente pour l'objectif principal de l'étude, à savoir la
quantification de l'effet rebond: 

- [Raynaud, M. (2014). Evaluation ex-post de l’efficacité de solutions de rénovation énergétique en résidentiel (Doctoral dissertation, Paris, ENMP).](https://www.theses.fr/2014ENMP0003): Les données sont un petit échantillon de 200 logements en Meuse et Haute Marne (peu représentatif). Le travail peut néanmoins être intéressant par sa manière de poser le problème.
  

- [Rapport de l'ADEME de 2022 sur la rénovation énergétique des bâtiments](https://librairie.ademe.fr/urbanisme-et-batiment/5845-financer-la-renovation-energetique-performante-des-logements.html) : Pose le cadre de la rénovation énergétique en France, et partant des objectifs de la stratégie nationale bas carbone, orientée tout BBC, élabore beaucoup de propositions, mais comportent assez peu de données ou de résultats.

- [Résultats de l'enquête TREMI, SDES-ADEME-ONRE](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2022-09/document_travail_61_reduction_ges_renovations_septembre2022_0.pdf): estime ainsi que les travaux réalisés en 2019 ont permis de diminuer de 2,1 MtCO 2eq les émissions de CO 2 conventionnelles du parc de maisons individuelles. Les postes de rénovation
qui génèrent les plus fortes réductions de gaz à effet de serre sont les changements de systèmes de chauffage (mise en place d‘un appareil de chauffage vertueux tel qu’une pompe à chaleur ou un système solaire) et la transition
d’une énergie fossile telle que le fioul à un système électrique ou au bois. L'étude fait des hypothèses de consommations moyennes et ne quantifie donc pas l'effet L'étude ne repose pas sur les données réelles de consommation. 

- [La rénovation énergétique des logements : bilan des travaux et des aides entre 2016 et 2019, ONRE, 2022](https://www.statistiques.developpement-durable.gouv.fr/la-renovation-energetique-des-logements-bilan-des-travaux-et-des-aides-entre-2016-et-2019-resultats): Combinaison de l'enquête TREMI et des données relatives aux aides à la rénovation énergétique de 2016 à 2019. Publication contenant des éléments qui
nous intéresse pour l'objectif secondaire (tableau 3). Ce tableau montre notamment que les rénovateurs sont plus souvent propriétaires dans de grosses agglomérations, sur des logements anciens et avec de forts revenus. Elle s'appuie comme les résultats de TREMI sur des hypothèses théoriques de réduction liées aux rénovations et non à une mesure directe de la consommation.

- Argumentaire sur les incitations à l'économie d'énergie par le CAS: [Comment limiter l’effet rebond des politiques énergétique dans le logement ?  L’importance des incitations comportementales », note d’analyse du Centre d’analyse stratégique n° 320, février 2013.](https://temis.documentation.developpement-durable.gouv.fr/docs/Temis/0077/Temis-0077764/Note_du_CAS_320.pdf)
  

- [L'Observatoire national de rénovation énergétique](https://www.ecologie.gouv.fr/observatoire-national-renovation-energetique#scroll-nav__2): Beaucoup de redirections vers le service statistique du MTES. Semble être plus une instance de concertation multi-partenaires que de production.

- [Cédric, B., Édouard, D., Benjamin, D., & Quentin, L. (2016). L'évaluabilité du Crédit d'Impôt pour la Transition
  Énergétique.](https://core.ac.uk/download/pdf/47355614.pdf) : Décrit les acteurs du CI, afin d'étudier l'évaluabilité de ce dispositif (enquête qualitative avec entretiens semi-directifs). 

### Références proches d'un point vue méthodologique mais non directements pertinentes

- Illustration de l'effet rebond dans le domaine de l'éclairage au UK : [Fouquet and Pearson, 2006](https://econpapers.repec.org/article/aenjournl/2006v27-01-a07.htm) 

- Revue de littérature narrative de l'effet rebond en prenant comme critère de jugement le PIB (economy-wide): [Brockway et al., 2021](https://www.sciencedirect.com/science/article/pii/S1364032121000769)


[^1]: Le Internal Rate of Return (à horizon 16 ans) avec les consommations projetées est de 12% contre -2.2% avec les mesures réelles. Si l'on se place au niveau sociétal, ie. en en monétisant les bénéfices de réduction de CO2 et en retranchant les transferts inter-individus dûs aux coûts fixes de connection au réseau (supporté par d'autres consommateurs), le IRR passe à -9.5%.